# Class to manage the timing of puppet-agent

class profile::base::puppetagent(
  $args   = '--onetime --no-daemonize',
  $offset = fqdn_rand(30),
) {
  cron { 'puppetagent':
    command  => "/opt/puppetlabs/puppet/bin/puppet agent ${args}",
    user     => 'root',
    month    => '*',
    monthday => '*',
    hour     => '*',
    minute   => [$offset, $offset + 30],
  }
}