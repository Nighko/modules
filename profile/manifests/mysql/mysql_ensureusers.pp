# Ensure specified users exist
# Requires https://forge.puppet.com/puppetlabs/mysql Module

class profile::mysql::mysql_ensureusers{
  include mysql
  mysql::db {'insomnia_main':
    user     => 'Nighko',
    password => 'MyPassword',
    host     => 'localhost',
    GRANT    => ['SELECT', 'UPDATE'],
    }
  mysql::db {'insomnia_main':
    user     => 'CFire',
    password => 'HisPassword',
    host     => 'localhost',
    GRANT    => ['SELECT', 'UPDATE'],
    }
}