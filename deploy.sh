#!/usr/bin/bash

for c in {1..4}
do
  ssh puppet.insomnia247.nl "cd /etc/puppetlabs/code/environments/production/modules && git pull && systemctl reload puppetserver" && break || sleep 3 && echo "retry"
done